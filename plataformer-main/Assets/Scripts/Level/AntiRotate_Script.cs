using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiRotate_Script : MonoBehaviour
{
    // El �ngulo objetivo de rotaci�n en grados
    private float targetAngle = 0f;

    // La velocidad de rotaci�n
    public float rotationSpeed = 90f; // grados por segundo

    // Variable para almacenar el �ngulo actual de rotaci�n
    private float currentAngle = 180f;

    public GameObject cameraObject;
    public GameObject plataform1;

    private float angleToRotate;

    private bool rotate;

    private void Start()
    {
        rotate = false;
    }

    void Update()
    {
        if (rotate)
        {
            angleToRotate = rotationSpeed * Time.deltaTime;

            // Aseg�rate de no rotar m�s all� del �ngulo objetivo
            if (currentAngle + angleToRotate > targetAngle)
            {
                angleToRotate = targetAngle - currentAngle;
            }

            cameraObject.transform.Rotate(0, 0, angleToRotate);
            //rotate = false;

            // Actualiza el �ngulo actual
            currentAngle += angleToRotate;

            // Desactiva el script si ha alcanzado el �ngulo objetivo
            if (currentAngle >= targetAngle)
            {
                enabled = false;
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player4")
        {
            rotate = true;
            StartCoroutine(Wait());
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);

        gameObject.SetActive(false);
        plataform1.gameObject.SetActive(false);
    }
}
