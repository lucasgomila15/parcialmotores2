using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving_Floor : MonoBehaviour
{
    public float speed = 5.0f; // Velocidad de movimiento
    public float height = 15f; // Altura del movimiento

    private Vector3 startPosition;

    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        float newY = Mathf.Sin(Time.time * speed) * height;
        transform.position = new Vector3(startPosition.x, startPosition.y + newY, startPosition.z);
    }
}
