using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Big : Controller_Player
{
    public float scaleIncrement = 0.1f;


    // Update se llama una vez por frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (GameManager.actualPlayer == 1)
            {
                Vector3 newScale = transform.localScale + new Vector3(scaleIncrement, scaleIncrement, scaleIncrement);
                transform.localScale = newScale;
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }

        if(GameManager.actualPlayer == 1) 
        {
            if (Input.GetKey(KeyCode.A))
            {
                rb.velocity = new Vector3(1 * -speed, rb.velocity.y, 0);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                rb.velocity = new Vector3(1 * speed, rb.velocity.y, 0);
            }
            else
            {
                rb.velocity = new Vector3(0, rb.velocity.y, 0);
            }
        }
    }
}
