Shader "Custom/Water"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
        _NormalMap ("Normal Map", 2D) = "bump" {}
        _WaveSpeed ("Wave Speed", Range(0, 1)) = 0.1
        _WaveScale ("Wave Scale", Range(0, 1)) = 0.1
        _ReflectionTex ("Reflection Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 200

        GrabPass { }

        CGPROGRAM
        #pragma surface surf Lambert alpha

        sampler2D _MainTex;
        sampler2D _NormalMap;
        sampler2D _ReflectionTex;
        float _WaveSpeed;
        float _WaveScale;
        float4 MainTex_ST;

        struct Input
        {
            float2 uv_MainTex;
            float3 viewDir;
        };

        half Time;

        void surf (Input IN, inout SurfaceOutput o)
        {
            // Main texture
            half2 uv = IN.uv_MainTex;

            // Wave effect
            half2 wave = uv + sin(uv.yx * 10 + Time * _WaveSpeed) * _WaveScale;
            fixed4 mainTex = tex2D(_MainTex, wave);

            // Normal map for waves
            fixed4 bump = tex2D(_NormalMap, wave);
            o.Normal = UnpackNormal(bump);

            // Reflection
            half3 reflectDir = reflect(-normalize(IN.viewDir), o.Normal);
            half4 reflectColor = tex2D(_ReflectionTex, reflectDir.xy * 0.5 + 0.5);

            // Combine
            o.Albedo = mainTex.rgb * 0.5 + reflectColor.rgb * 0.5;
            o.Alpha = mainTex.a;
        }
        ENDCG
    }

    Fallback "Diffuse"
}
