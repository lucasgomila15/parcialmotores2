// Upgrade NOTE: commented out 'float4x4 _Object2World', a built-in variable

Shader "Custom/MovingSky"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
        _Speed ("Speed", Range(0.1, 2.0)) = 1.0
    }
    SubShader
    {
        Tags { "Queue" = "Background" "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            sampler2D _MainTex;
            float _Speed;
            float4 _MainTex_ST;
            // float4x4 _Object2World;

            struct appdata_t
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                // Manual UV transformation
                o.uv = v.texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
                return o;
            }

            float Time;

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv;
                uv.x += Time * _Speed;
                fixed4 col = tex2D(_MainTex, uv);
                return col;
            }
            ENDCG
        }
    }
    Fallback "Diffuse"
}
